import os

from django.db import models
from django.contrib.auth.models import User
from django.conf import settings


class File(models.Model):
    md5 = models.CharField(max_length=32, unique=True)

    @property
    def filepath(self):
        return os.path.join(settings.STORAGE_FOLDER, self.md5)


class Link(models.Model):
    name = models.CharField(max_length=255, verbose_name="имя файла")
    file = models.ForeignKey(File, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

    class Meta:
        verbose_name = "ссылка на файл"
        verbose_name_plural = "ссылки на файлы"
        unique_together = (('user', 'name'), )
