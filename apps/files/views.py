import os
import hashlib
import mimetypes
import urllib
from wsgiref.util import FileWrapper

from django.shortcuts import render, redirect, get_object_or_404
from django.conf import settings
from django.http import StreamingHttpResponse, Http404
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from .models import Link, File

from .forms import FileUploadForm


@login_required
def upload_file(request):
    if request.method == "POST":
        form = FileUploadForm(request.POST, request.FILES)
        if form.is_valid():
            file_source = request.FILES['file']

            md5 = hashlib.md5()
            for chunk in iter(lambda: file_source.read(4096), b""):
                md5.update(chunk)

            try:
                file = File.objects.get(md5=md5.hexdigest())
            except File.DoesNotExist:
                path = os.path.join(settings.STORAGE_FOLDER, md5.hexdigest())
                with open(path, 'wb+') as filepath:
                    for chunk in file_source.chunks():
                        filepath.write(chunk)
                file = File.objects.create(md5=md5.hexdigest())

            try:
                link = Link.objects.get(user=request.user, file=file)
                messages.add_message(request, messages.INFO, "Данный файл уже загружен вами под именем %s" % link.name)
            except Link.DoesNotExist:
                Link.objects.create(user=request.user, file=file, name=file_source)
            return redirect('dashboard')
    else:
        form = FileUploadForm()
        return render(request, "files/upload_file.html", {"form": form})


@login_required
def delete_file(request, file_md5):
    file_instance = get_object_or_404(File, md5=file_md5)
    link = get_object_or_404(Link, user=request.user, file=file_instance)
    if request.method == "POST":
        link.delete()
        file_instance.refresh_from_db()
        if file_instance.link_set.count() == 0:
            file_instance.delete()
            os.remove(file_instance.filepath)
        return redirect('dashboard')
    else:
        return render(request, 'files/delete_confirm.html', {"obj": link})


@login_required
def download_file(request, file_md5):
    file_instance = get_object_or_404(File, md5=file_md5)
    link = get_object_or_404(Link, user=request.user, file=file_instance)
    if os.path.exists(file_instance.filepath):
        response = StreamingHttpResponse(FileWrapper(open(file_instance.filepath, 'rb'), 8192),
                                         content_type=mimetypes.guess_type(file_instance.filepath)[0])
        response['Content-Length'] = os.path.getsize(file_instance.filepath)
        response['Content-Disposition'] = "attachment; filename={}".format(urllib.parse.quote_plus(link.name))
        return response
    else:
        raise Http404
