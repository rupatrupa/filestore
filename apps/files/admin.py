from django.contrib import admin

from .models import File, Link

admin.site.register(File)
admin.site.register(Link)
