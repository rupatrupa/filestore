from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^upload_file/$', views.upload_file, name='upload_file'),
    url(r'^delete_file/(?P<file_md5>[\w-]+)/$', views.delete_file, name='delete_file'),
    url(r'^download_file/(?P<file_md5>[\w-]+)/$', views.download_file, name='download_file')
]
