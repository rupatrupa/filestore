from django.contrib import admin

from .models import SharedLink


admin.site.register(SharedLink)
