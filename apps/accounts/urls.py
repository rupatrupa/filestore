from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.dashboard, name='dashboard'),
    url(r'^share_link/(?P<file_md5>[\w-]+)/$', views.share_link, name='public_share_link'),
    url(r'^delete_share_link/(?P<file_md5>[\w-]+)', views.delete_shared_link, name='delete_shared_link'),
    url(r'^public_download/(?P<url_suffix>[\w-]+)', views.public_download, name='public_download')
]
