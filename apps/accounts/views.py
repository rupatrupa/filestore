import os
import random
import string
import mimetypes
import urllib
from wsgiref.util import FileWrapper


from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import StreamingHttpResponse, Http404

from files.models import Link

from .models import SharedLink


def register(request):
    if request.method == "POST":
        form = UserCreationForm(data=request.POST)
        if form.is_valid():
            form.save()

            user = authenticate(username=form.cleaned_data.get('username'), password=form.cleaned_data.get('password1'))
            login(request, user)
            return redirect('dashboard')
    else:
        form = UserCreationForm()
    return render(request, 'registration/register.html', {'form': form})


@login_required
def dashboard(request):
    return render(request, "accounts/dashboard.html",
                  {"links": request.user.link_set.select_related('user', 'file').all()})


@login_required
def share_link(request, file_md5):
    link = get_object_or_404(Link, user=request.user, file__md5=file_md5)
    try:
        shared_link = SharedLink.objects.get(link=link)
    except SharedLink.DoesNotExist:
        url_suffix = ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(255))
        shared_link = SharedLink.objects.create(link=link, url_suffix=url_suffix)
    return render(request, 'accounts/share_file.html', {'shared_link': shared_link})


@login_required
def delete_shared_link(request, file_md5):
    link = get_object_or_404(Link, user=request.user, file__md5=file_md5)
    shared_link = get_object_or_404(SharedLink, link=link)
    shared_link.delete()
    messages.add_message(request, messages.INFO, "Ссылка успешно удалена")
    return redirect('dashboard')


def public_download(request, url_suffix):
    shared_link = get_object_or_404(SharedLink, url_suffix=url_suffix)
    link = shared_link.link
    file_instance = shared_link.link.file
    if os.path.exists(file_instance.filepath):
        response = StreamingHttpResponse(FileWrapper(open(file_instance.filepath, 'rb'), 8192),
                                         content_type=mimetypes.guess_type(file_instance.filepath)[0])
        response['Content-Length'] = os.path.getsize(file_instance.filepath)
        response['Content-Disposition'] = "attachment; filename={}".format(urllib.parse.quote_plus(link.name))
        return response
    else:
        raise Http404
