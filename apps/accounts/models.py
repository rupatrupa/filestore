from django.db import models
from files.models import Link


class SharedLink(models.Model):
    url_suffix = models.CharField(max_length=255, db_index=True)
    link = models.OneToOneField(Link, on_delete=models.CASCADE)
