Инструкция по разворачиванию проекта:

1. Обновить базу данных пакетов репозиториев linux с помощью команды `sudo apt-get update`
2. Установить необходимые зависимости для python3:
    `sudo apt-get install python3-pip python3-dev libpq-dev`
3. Установить менеджер виртуальных окружений для python с помощью команды `sudo -H pip3 install`
4. Клонировать проект из репозитория с помощью команды git clone 
5. Перейти в терминале в папку с проектом и создать виртуальное окружение с помощью команды `virtualenv env`
6. Активировать виртуальное окружение source env/bin/activate/
7. Установить необходимые зависимости через команду pip install -r requirements/production.txt
8. Создать файл production.env, в нем указать переменную `SECRET_KEY=<secret_key>`
9. Выполнить команду python manage.py migrate для создания файла БД db.sqlite3 и таблиц для БД
10. Выполнить команду python manage.py collectstatic для сбора статики всего проекта в отдельную папку, на которую будет настроен nginx.
11. Создать служебный файл gunicorn.service командой sudo nano `/etc/systemd/system/gunicorn.service` со следующим содержимым

          [Unit]
          Description=filestore gunicorn daemon
          
          After=network.target
          
          [Service]
          
          User=username
          
          Group=www-data
          
          WorkingDirectory=/<путь_до_папки_с_проектом>/filestore/
          
          Environment="DJANGO_SETTINGS_MODULE=config.settings.production"
          
          ExecStart=/<путь_до_папки_с_проектом>/filestore/bin/gunicorn --access-logfile --bind unix:/<путь_до_папиа_с_проектом>/filestore/gunicorn.sock config.wsgi:application
          
          [Install]
          WantedBy=multi-user.target


12. Запустить процесс gunicorn и активировать запуск его при загрузке следующими командами
    `sudo systemctl enable gunicorn`

    `sudo systemctl start gunicorn`

13. Установить nginx командой sudo apt-get install nginx

14. Создать файл для конфигурации блока nginx sudo nano /etc/nginx/sites-available/filestore со следующим содержимым


    server {
    
        listen 80;
        server_name <IP_адрес_сервера>;
    
    	location /static/ {
        	alias/<путь_до_папки_с_проектом>/filestore/collectedstatic/;
    	}
    
        location / {
            include proxy_params;
            proxy_pass http://unix:/<путь_до_папки_с_проектом>/filestore/gunicorn.sock;
        }
    }

15. Выполнить привязку созданного блока nginx приложения к директории sites-enabled и перезапустить nginx:
    `sudo ln -s /etc/nginx/sites-available/myproject /etc/nginx/sites-enabled`

    `sudo systemctl restart nginx`