from .base import *

import environ

DEBUG = False
env = environ.Env()
env.read_env("production.env")
SECRET_KEY = env('SECRET_KEY')
ALLOWED_HOSTS = ['*', ]
