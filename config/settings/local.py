from .base import *


DEBUG = True

INSTALLED_APPS += ['debug_toolbar']
MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware', ]

ALLOWED_HOSTS += [
    "localhost",
    "0.0.0.0",
    "127.0.0.1",
]
INTERNAL_IPS = ALLOWED_HOSTS
